---
layout: page
title: Ideas
permalink: /en/ideas/
---
This is list of good projects, ideas and organisatioins witch have my support 
and may change the world in the way that I'd like to. This is the future that we need.
Join if you can.

# Social ideas:
* Caffѐ sospeso / Suspended Coffee / [Kava na zalogo](http://kavanazalogo.blogspot.si/)
* Kruh za prjatla

# Futuristic ideas:
* [Projekt Venus](https://www.thevenusproject.com/)
* [foodisfreeproject.org](http://foodisfreeproject.org/)
* [Open Street Map](https://www.openstreetmap.org/)
* [_wlan slovenija_](https://wlan-si.net/)
* [Unconditional basic income](http://www.basicincome.org/)

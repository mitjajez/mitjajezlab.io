---
layout: page
title: Programska oprema
permalink: /programje/
---

# Namizni računalnik
* OS/Distribucija: GNU/Linux - Debian
* Namizje: KDE
* Spletni brskalnik: Mozilla Firefox
* Bralnik pošte: Mozilla Thunderbird
* Pisarniški paket: Texlive, LibreOffice
* Video predvajalnik: VLC
* Matematični paket: Octave
* Mapping: JOSM, QGIS in drugi
* Razvojna orodja: PyCharm, Atom in drugi
* Virtualizacija: Docker, VirtualBox
* 3D modeling: FreeCAD, OpenSCAD
* Electronics: KiCAD, STM32CubeIDE in drugi

# Telefon
* Operacijski sistem: Android; (TODO: LineageOS, KDE)
* Katalog odprtokodnih programov: F-droid
* Bralnik SMSov: Signal
* Spletni brskalnik: Firefox for Android
* Bralnik e-pošte: K-9 Mail
* Navigator: OsmAnd~
* Video predvjalnik: VLC
* Datotečni brskalnik: Amaze

# Beaglebone Black
* Ostaja na originalnem sistemu
